CHAR_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
CHAR_LOWERCASE = "abcdefghijklmnopqrstuvwxyz"

class ROT13:
    @staticmethod 
    def encode(original_string, rotNum = 13):
        # Finding length of the original string
        i = 0
        encoded_array = []
        for i in range(len(original_string)):
            n = original_string[i]
            if n in CHAR_UPPERCASE:
                k = CHAR_UPPERCASE.find(n)
                k = k + rotNum
                k = k % 26
                encoded_array.append(CHAR_UPPERCASE[k])
            elif n in CHAR_LOWERCASE:
                k = CHAR_LOWERCASE.find(n)
                k = k + rotNum
                k = k % 26
                encoded_array.append(CHAR_LOWERCASE[k])
            else:
                encoded_array.append(n)
        return "".join(encoded_array)

    @staticmethod
    def decode(encoded_string, rotNum = 13):
        i = 0
        decoded_array = []
        for i in range(len(encoded_string)):
            n = encoded_string[i]
            if n in CHAR_UPPERCASE:
                k = CHAR_UPPERCASE.find(n)
                k = k - rotNum
                k = k % 26
                decoded_array.append(CHAR_UPPERCASE[k])
            elif n in CHAR_LOWERCASE:
                k = CHAR_LOWERCASE.find(n)
                k = k - rotNum
                k = k % 26
                decoded_array.append(CHAR_LOWERCASE[k])
            else:
                decoded_array.append(n)
        return "".join(decoded_array)

def main():
    print("Starting the program.\n")
    original_string = input("Give a string: ")
    print("Original string:", original_string)
    encoded_string = ROT13.encode(original_string)
    print("Encoded:", encoded_string)
    decoded_string = ROT13.decode(encoded_string)
    print("Decoded:", decoded_string)
    print("\nProgram ends.")

if __name__=="__main__":
    main()
