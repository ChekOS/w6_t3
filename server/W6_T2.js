let CHAR_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
CHAR_LOWERCASE = "abcdefghijklmnopqrstuvwxyz";

function mod(n, m) {
    return ((n % m) + m) % m;
}

class ROT13 {
    static encode(original_string, rotNum = 13) {
        let l = original_string.length, n, k, encoded_string;
        const inter_string = [];
        for (let i = 0; i <= original_string.length; i++) {
            n = original_string[i];
            if (CHAR_UPPERCASE.includes(n) == true) {
                k = CHAR_UPPERCASE.search(n);
                k = k + rotNum;
                k = k % 26;
                inter_string.push(CHAR_UPPERCASE[k]);
            }
            else if (CHAR_LOWERCASE.includes(n) == true) {
                k = CHAR_LOWERCASE.search(n);
                k = k + rotNum;
                k = k % 26;
                inter_string.push(CHAR_LOWERCASE[k]);
            }
            else {
                inter_string.push(n);
            }
        }
        encoded_string = inter_string.join("");
        return encoded_string;
    }

    static decode(encoded_string, rotNum = 13) {
        let l = encoded_string.length, n, k, decoded_string;
        const inter_string = [];
        l = encoded_string.length;
        for (let i = 0; i <= l; i++) {
            n = encoded_string[i];
            if (CHAR_UPPERCASE.includes(n) == true) {
                k = CHAR_UPPERCASE.search(n);
                k = k - rotNum;
                k = mod(k, 26);
                inter_string.push(CHAR_UPPERCASE[k]);
            }
            else if (CHAR_LOWERCASE.includes(n) == true) {
                k = CHAR_LOWERCASE.search(n);
                k = k - rotNum;
                k = mod(k, 26);
                inter_string.push(CHAR_LOWERCASE[k]);
            }
            else {
                inter_string.push(n);
            }
        }
        decoded_string = inter_string.join("");
        return decoded_string;
    }
}

const readline = require('readline');
const ask = async (question) => new Promise(
    resolve => {
        const rl = readline.createInterface(process.stdin, process.stdout);
        rl.question(
            question,
            (input) => {
                rl.close();
                resolve(input);
            }
        )
    }
)

const main = async () => {
    console.log("Starting the program.\n");
    let orig_string = await ask("Input string: ");
    console.log("Original string:", orig_string);
    let encoded_string = ROT13.encode(orig_string);
    console.log("Encoded:", encoded_string);
    let decoded_string = ROT13.decode(encoded_string);
    console.log("Decoded:", decoded_string);
    console.log("\nProgram ends.")
}

// main()

module.exports = { ROT13 }
