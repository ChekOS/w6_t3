const express = require('express')
const ROT13 = require('./W6_T2.js')
const app = express()
const port = 3000

app.listen(port, () => console.log(`Listening on port ${port}...`));

app.get('/', (req,res) => {
    const encoded_string = req.query.encoded_string;
    console.log("\nRecieved the payload: " + encoded_string);
    decoded_string = ROT13.ROT13.decode(encoded_string);
    console.log("Decoded string: " + decoded_string);
    console.log("\nProgram ends.")
})