import requests
from W6_T1 import ROT13

schema = 'http://'
host = 'localhost'
port = 3000

class Endpoint:
  def __init__(self, path, method, params):
    self.path = path
    self.method = method
    self.params = params

def main():
    print("Starting the program.\n")
    original_string = input("Give a string: ")
    print("Original string: ", original_string)
    encoded_string = ROT13.encode(original_string)
    print("Encoded string: ", encoded_string)

    e = Endpoint('/', 'GET', {
        'encoded_string': encoded_string
    })    
    url = schema + host + ':' + str(port) + e.path
    try:
        requests.get(url, params = e.params, timeout=1)
    except:
        pass

    print("Request is sent.")


if __name__=="__main__":
    main()